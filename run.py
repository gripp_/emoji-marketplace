import json
import logging
from multiprocessing import freeze_support

from discord_io import DiscordIO
from market import Market

if __name__ == '__main__':
  logging.basicConfig(level=logging.DEBUG)

  with open('trader.config', 'r') as config_file:
    config = json.load(config_file)
    TOKEN = config['token']

  logging.debug(f'Config loaded. Token is [redacted].') # {TOKEN}.')

  io = DiscordIO(TOKEN)
  market = Market(io)

  while not market.ready:
    pass

  market.start()
