from enum import Enum

Keys = Enum(
    'Keys',
    map(
        lambda k : (k, f'{k.lower()}'),
        [
          'AUTHOR',
          'CHANNEL',
          'CHANNELS',
          'CONTENT',
          'ID',
          'MEMBERS',
          'MESSAGE',
          'READY',
          'TIMESTAMP',
          'USERNAME',
        ]))
