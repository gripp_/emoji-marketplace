import color

class Player(object):
  def __init__(self, pid):
    self._coins = 100
    self._gallery = set()
    self._hand = set()
    self._id = pid

  @property
  def coins(self):
    return self._coins

  @property
  def gallery(self):
    if len(self._gallery) <= 0:
      return '❌'
    return ''.join(color.to_text(c.color) for c in self._gallery)

  @property
  def hand(self):
    return self._hand

  @property
  def id(self):
    return self._id

  def add_to_gallery(self, card):
    return self._gallery.add(card)

  def profit(self, amount):
    self._coins -= amount

  def spend(self, amount):
    self._coins -= amount

  def sell_paintings(self, color_values):
    for card in self._gallery:
      if card.color in color_values:
        self._coins += color_values[card.color]

  def take_card(self, card):
    return self._hand.add(card)
