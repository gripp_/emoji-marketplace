# Getting Started with Emoji Marketplace

Author: gripp (marshall@glasseyeballs.com)

## 1/ Read the contributor agreement and the license agreement.

They are packaged with this repository as CONTRIBUTORS.md and LICENSE.md respectively.

## 2/ Get a bot token

[This website](https://www.writebots.com/discord-bot-token/) will walk you through getting a bot token. Once you do, you'll need to create a new file called `trader.config` and put the following in it:

```
{
  "token": "(put your bot token here)"
}
```

## 3/ Add your bot to a server

[This website](https://discordjs.guide/preparations/adding-your-bot-to-servers.html#bot-invite-links) will walk you through that.

## 4/ Install the Discord API

You should be able to do that by running this command: `python3 -m pip install -U discord.py`

## 5/ Run the program

You should be able to do that by running this command: `python3 run.py`

## 6/ Interact with the bot

The bot should not be able to receive and respond to your messages, so try playing a game with it. The exact messages it will respond to are still in development and thus changing frequently and thus unstable, so I won't attempt to document them here. But try this: `@(bot name) new game @player1 @player2 @player3`
