from enum import Enum

class SaleType(Enum):
  DOUBLE_UP = 1
  FRENZY = 2
  ONE_SHOT = 3
  PRICETAG = 4
  PROPOSAL = 5

def to_text(sale_type):
  if sale_type == SaleType.DOUBLE_UP:
    return '♊️'
  elif sale_type == SaleType.FRENZY:
    return '🎊'
  elif sale_type == SaleType.ONE_SHOT:
    return '🔂'
  elif sale_type == SaleType.PRICETAG:
    return '🏷'
  elif sale_type == SaleType.PROPOSAL:
    return '📨'
  else:
    raise Exception('Invalid sale type.')
