import logging

from commerce import Commerce

class Market(object):
  def __init__(self, io):
    self._io = io
    self._commerce = None

  @property
  def ready(self):
    return self._io.ready

  def start(self):
    logging.info('Starting market.')
    while self._commerce is None:
      message = self._io.next_message
      if message is None:
        continue

      msg_content, msg_author, msg_channel, _ = message

      words = msg_content.split()

      if len(words) < 6 or len(words) > 7:
        continue

      user_info = self._get_user_info(words[0])
      addressed_to_trader = (
          user_info is not None and user_info[0] == self._io.bot_tag)
      is_new_game_request = words[1:3] == [ 'new', 'game' ]
      players = [
          self._io.scrape_id(word)
          for word
          in words[3:]
          if self._get_user_info(word) is not None
      ]

      if (
          addressed_to_trader and
          is_new_game_request and
          all([ p is not None for p in players ])):
        logging.info(
            f'{self._io.tag(msg_author)} requested a new game with '
            f'{" ".join([ str(p) for p in players ])}.')

        self._io.public_channel_id = msg_channel[1]
        self._commerce = Commerce(players, self._io)
    self._commerce.start()

  def _get_user_info(self, tag):
    pid = self._io.scrape_id(tag)
    if pid is None:
      return None
    return self._io.get_user_info(pid)
