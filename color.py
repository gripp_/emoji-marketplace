from enum import Enum

class Color(Enum):
  ORANGE = 1
  GREEN = 2
  RED = 3
  BLUE = 4
  YELLOW = 5

def to_text(color):
  if color == Color.ORANGE:
    return '🟠'
  elif color == Color.GREEN:
    return '🟢'
  elif color == Color.RED:
    return '🔴'
  elif color == Color.BLUE:
    return '🔵'
  elif color == Color.YELLOW:
    return '🟡'
  else:
    raise Exception('Invalid color.')
