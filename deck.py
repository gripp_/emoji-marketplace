import random

from card import Card
from color import Color
from sale_type import SaleType

class Deck(object):
  def __init__(self):
    self._deck = [
        Card(1, Color.YELLOW, SaleType.DOUBLE_UP),
        Card(2, Color.YELLOW, SaleType.DOUBLE_UP),
        Card(3, Color.YELLOW, SaleType.FRENZY),
        Card(4, Color.YELLOW, SaleType.FRENZY),
        Card(5, Color.YELLOW, SaleType.FRENZY),
        Card(6, Color.YELLOW, SaleType.ONE_SHOT),
        Card(7, Color.YELLOW, SaleType.ONE_SHOT),
        Card(8, Color.YELLOW, SaleType.ONE_SHOT),
        Card(9, Color.YELLOW, SaleType.PRICETAG),
        Card(10, Color.YELLOW, SaleType.PRICETAG),
        Card(11, Color.YELLOW, SaleType.PROPOSAL),
        Card(12, Color.YELLOW, SaleType.PROPOSAL),

        Card(13, Color.BLUE, SaleType.DOUBLE_UP),
        Card(14, Color.BLUE, SaleType.DOUBLE_UP),
        Card(15, Color.BLUE, SaleType.FRENZY),
        Card(16, Color.BLUE, SaleType.FRENZY),
        Card(17, Color.BLUE, SaleType.FRENZY),
        Card(18, Color.BLUE, SaleType.ONE_SHOT),
        Card(19, Color.BLUE, SaleType.ONE_SHOT),
        Card(20, Color.BLUE, SaleType.PRICETAG),
        Card(21, Color.BLUE, SaleType.PRICETAG),
        Card(22, Color.BLUE, SaleType.PRICETAG),
        Card(23, Color.BLUE, SaleType.PROPOSAL),
        Card(24, Color.BLUE, SaleType.PROPOSAL),

        Card(25, Color.RED, SaleType.DOUBLE_UP),
        Card(26, Color.RED, SaleType.DOUBLE_UP),
        Card(27, Color.RED, SaleType.FRENZY),
        Card(28, Color.RED, SaleType.FRENZY),
        Card(29, Color.RED, SaleType.FRENZY),
        Card(30, Color.RED, SaleType.ONE_SHOT),
        Card(31, Color.RED, SaleType.ONE_SHOT),
        Card(32, Color.RED, SaleType.ONE_SHOT),
        Card(33, Color.RED, SaleType.PRICETAG),
        Card(34, Color.RED, SaleType.PRICETAG),
        Card(35, Color.RED, SaleType.PRICETAG),
        Card(36, Color.RED, SaleType.PROPOSAL),
        Card(37, Color.RED, SaleType.PROPOSAL),
        Card(38, Color.RED, SaleType.PROPOSAL),

        Card(39, Color.GREEN, SaleType.DOUBLE_UP),
        Card(40, Color.GREEN, SaleType.DOUBLE_UP),
        Card(41, Color.GREEN, SaleType.DOUBLE_UP),
        Card(42, Color.GREEN, SaleType.FRENZY),
        Card(43, Color.GREEN, SaleType.FRENZY),
        Card(44, Color.GREEN, SaleType.FRENZY),
        Card(45, Color.GREEN, SaleType.ONE_SHOT),
        Card(46, Color.GREEN, SaleType.ONE_SHOT),
        Card(47, Color.GREEN, SaleType.ONE_SHOT),
        Card(48, Color.GREEN, SaleType.PRICETAG),
        Card(49, Color.GREEN, SaleType.PRICETAG),
        Card(50, Color.GREEN, SaleType.PRICETAG),
        Card(51, Color.GREEN, SaleType.PROPOSAL),
        Card(52, Color.GREEN, SaleType.PROPOSAL),
        Card(53, Color.GREEN, SaleType.PROPOSAL),

        Card(54, Color.ORANGE, SaleType.DOUBLE_UP),
        Card(55, Color.ORANGE, SaleType.DOUBLE_UP),
        Card(56, Color.ORANGE, SaleType.DOUBLE_UP),
        Card(57, Color.ORANGE, SaleType.FRENZY),
        Card(58, Color.ORANGE, SaleType.FRENZY),
        Card(59, Color.ORANGE, SaleType.FRENZY),
        Card(60, Color.ORANGE, SaleType.FRENZY),
        Card(61, Color.ORANGE, SaleType.ONE_SHOT),
        Card(62, Color.ORANGE, SaleType.ONE_SHOT),
        Card(63, Color.ORANGE, SaleType.ONE_SHOT),
        Card(64, Color.ORANGE, SaleType.PRICETAG),
        Card(65, Color.ORANGE, SaleType.PRICETAG),
        Card(66, Color.ORANGE, SaleType.PRICETAG),
        Card(67, Color.ORANGE, SaleType.PROPOSAL),
        Card(68, Color.ORANGE, SaleType.PROPOSAL),
        Card(69, Color.ORANGE, SaleType.PROPOSAL),
    ]

    random.shuffle(self._deck)

  def draw(self):
    return self._deck.pop(0)
