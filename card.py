import color
import sale_type

class Card(object):
  def __init__(self, id_num, color, sale_type):
    self._id = id_num
    self._color = color
    self._sale_type = sale_type

  @property
  def color(self):
    return self._color

  @property
  def id(self):
    return self._id

  @property
  def sale_type(self):
    return self._sale_type

  @property
  def text(self):
    return f'{color.to_text(self._color)}{sale_type.to_text(self._sale_type)}'
