import logging
import random
from queue import SimpleQueue

from color import Color
from color import to_text as color_to_text
from deck import Deck
from player import Player
from sale_type import SaleType
from sale_type import to_text as sale_to_text

class Commerce(object):
  _CARDS_PER_ROUND = {
      # (num_players, round): num_cards
      (3, 0): 10,
      (3, 1): 6,
      (3, 2): 6,
      (3, 3): 0,

      (4, 0): 9,
      (4, 1): 4,
      (4, 2): 4,
      (4, 3): 0,

      (5, 0): 8,
      (5, 1): 3,
      (5, 2): 3,
      (5, 3): 0,
  }

  def __init__(self, players, io):
    if len(players) not in [3, 4, 5]:
      raise Exception('Invalid number of players.')

    self._deck = Deck()
    self._io = io
    self._turn_order = SimpleQueue()
    self._players = [ Player(p) for p in players ]
    self._id_to_player = { p.id: p for p in self._players }
    self._round = 0
    self._color_values = {
      Color.ORANGE: 0,
      Color.GREEN: 0,
      Color.RED: 0,
      Color.BLUE: 0,
      Color.YELLOW: 0,
    }

    random.shuffle(self._players)

    for p in self._players:
      self._turn_order.put(p)

    logging.info('Commerce initialized.')

  @property
  def coin_emoji(self):
    return '💎'

  def complete_auction(self, cards, winner, price, sellers):
    winner.spend(price)

    for seller, _ in sellers:
      if seller != winner:
        seller.profit(int(price // len(sellers)))

    for c in cards:
      winner.add_to_gallery(c)

    seller_tags = " and ".join([ tag for _, tag in sellers ])
    card_tags = " and ".join([ c.text for c in cards ])
    winner_tag = self._io.tag(winner.id)
    self._io.public_send(
        [f'{seller_tags} sold {card_tags} to {winner_tag} for {price}!'])

  def start(self):
    logging.info('Commerce started.')

    colors = [
        Color.ORANGE, Color.GREEN, Color.RED, Color.BLUE, Color.YELLOW, ]

    welcomes = ' '.join(
        [f'welcome {self._io.tag(p.id)}!' for p in self._players])
    self._io.public_send([
            (
              f'hello! my name is {self._io.bot_tag}. i am an emoji profiteer '
              'from the far-off land of-'
            ),
            'whats that? youve never heard of emoji trading?',
            (
              'thats perfect! in this unsuspecting new land, ill have a '
              'monopoly on the commodified emoji market!'
            ),
            f'you all can be the first emoji traders. {welcomes}',
            (
              'first things first, there are five emoji colors: '
              f'{"".join([color_to_text(c) for c in colors])}'
            ),
            (
              'each emoji can only be sold in one specific way and there are '
              'five different types of emoji sales...'
            ),
            (
              f'a {sale_to_text(SaleType.PRICETAG)} sale will let the '
              'seller name a price for their emoji.'
            ),
            (
              f'a {sale_to_text(SaleType.ONE_SHOT)} sale gives each trader '
              'one chance to make their best offer.'
            ),
            (
              f'with a {sale_to_text(SaleType.PROPOSAL)} sale, each trader '
              'makes a secret offer.'
            ),
            (
              f'a {sale_to_text(SaleType.FRENZY)} sale is like an auction. '
              'traders make public bids until the seller accepts one.'
            ),
            (
              f'finally, a {sale_to_text(SaleType.DOUBLE_UP)} sale will '
              'let the seller (or another player if they dont want to) select '
              'second emoji of the same color to auction. the emoji will be '
              'sold as a package according to the sale type of the second one.'
            ),
            (
              'well trade until five emoji of one color have been put on the '
              'market. then, ill buy back all the emoji of the three most '
              'popular colors.'
            ),
            (
              'more popular colors are worth more! well play four rounds and '
              'emoji colors accumulate value each round.'
            ),
            (
              'ill give everyone '
              f'{Commerce._CARDS_PER_ROUND[(len(self._players), 0)]} emoji '
              f'and 100{self.coin_emoji} to start out.'
            ),
            (
              'ill also keep track of the trading rules (and more importantly '
              f'everyones {self.coin_emoji}...)'
            ),
            (
              'lets get started! check your private messages to see your '
              'starting portfolio.'
            ),
        ])

    while self._round < 4:
      for _ in range(
          Commerce._CARDS_PER_ROUND[(len(self._players), self._round)]):
        for player in self._players:
          player.take_card(self._deck.draw())

      played_cards = {
          Color.ORANGE: 0,
          Color.GREEN: 0,
          Color.RED: 0,
          Color.BLUE: 0,
          Color.YELLOW: 0,
      }

      while all([ n < 5 for n in played_cards.values() ]):
        self._send_game_state()

        current_player = self._turn_order.get()

        # This will make all turns belong to @gripp.
        # while current_player.id != 608308248155783168:
        #   self._turn_order.put(current_player)
        #   current_player = self._turn_order.get()

        current_player_tag = self._io.tag(current_player.id)

        logging.info(f'Next player ID: {current_player.id}.')

        sellers = [ ( current_player, current_player_tag ) ]

        cards = [
            self._select_card_from_hand(
                'which emoji would you like to put on the market?',
                current_player)
        ]

        played_cards[cards[0].color] += 1
        if played_cards[cards[0].color] >= 5:
          break

        if cards[0].sale_type == SaleType.DOUBLE_UP:
          second_card = self._select_card_from_hand(
              'what card do you want to add to the sale?',
              current_player,
              allow_none=True,
              color_restriction=cards[0].color,
              exclude_doubles=True)
          while second_card is None:
            self._turn_order.put(current_player)
            current_player = self._turn_order.get()
            current_player_tag = self._io.tag(current_player.id)

            if (current_player, current_player_tag) in sellers:
              self.complete_auction(cards, current_player, 0, current_player)
              continue

            second_card = self._select_card_from_hand(
                (f'{sellers[0][1]} wants to offer {cards[0].text}. which card '
                 'do you want to add to the sale?'),
                current_player,
                allow_none=True,
                color_restriction=cards[0].color)
          if sellers[0][0] != current_player:
            sellers += [ ( current_player, current_player_tag ) ]
          cards += [ second_card ]
          played_cards[second_card.color] += 1
          if played_cards[second_card.color] >= 5:
            break

        offering_msg  = (
            f'{current_player_tag} offers {cards[0].text}.'
            if len(sellers) == 1
            else (
                f'{sellers[0][1]} and {sellers[1][1]} offer {cards[0].text} '
                f'and {cards[1].text}.'))
        self._io.public_send([offering_msg])

        bid_order = SimpleQueue()
        current_index = (
            self._players.index(current_player) + 1) % len(self._players)

        while bid_order.qsize() < len(self._players):
          player = self._players[current_index]
          bid_order.put(( player, self._io.tag(player.id) ))
          current_index = (current_index + 1) % len(self._players)

        sale_type = cards[-1].sale_type

        if played_cards[cards[0].color] < 5:
          winner_id = None
          price = None

          if sale_type == SaleType.FRENZY:
            winner_id, price = self._frenzy(current_player)

          elif sale_type == SaleType.ONE_SHOT:
            winner_id, price = self._one_shot(current_player, bid_order)

          elif sale_type == SaleType.PRICETAG:
            winner_id, price = self._pricetag(
                (current_player, current_player_tag), bid_order)

          else:
            winner_id, price = self._proposal(current_player, cards, bid_order)

          logging.info([p.id for p in self._players])
          logging.info(winner_id)
          winner = [ p for p in self._players if p.id == winner_id ][0]

          self.complete_auction(cards, winner, price, sellers)

        self._turn_order.put(current_player)

      colors.sort(
          cmp=(
            lambda c1, c2 : (
                played_cards[c1] < played_cards[c2] or
                (
                  played_cards[c1] == played_cards[c2] and
                  c1.value < c2.value
                ))))

      self._color_values[colors[0]] += 30
      self._color_values[colors[1]] += 20
      self._color_values[colors[2]] += 10

      round_values = {
          color: value
          for color, value
          in self._color_values.items()
          if color in [ colors[0], colors[1], colors[2] ]
      }

      for player in self._players:
        player.sell_paintings(self._color_values, round_values)

      self._round += 1

    scores = '... '.join(
        [ f'{self._io.tag(p.id)} has {p.coins}' for p in self._players ])
    self._io.public_send([f'okay! thats game! {scores[:-2]}'])

  def _frenzy(self, current_player):
    current_player_tag = self._io.tag(current_player.id)
    self._io.public_send(
        [
          f'anyone can now bid! including {current_player_tag}',
          (
            f'{current_player_tag} can end the auction by sending three '
            'messages: first `going` then `going` then `gone`.'
          ),
          (
            f'if anyone bids while {current_player_tag} is trying to end the '
            'bidding, itll reset the count...'
          ),
        ])

    going = 0
    gone = False
    winner_id = None
    price = 0

    while not gone:
      next_message = self._io.next_message

      if next_message is None:
        continue

      msg_content, msg_author, _, _ = next_message
      words = msg_content.split()

      msg_is_from_current_player = msg_author == current_player.id

      if (
          msg_is_from_current_player and
          msg_content == 'gone' and
          going >= 2):
        gone = True
      elif msg_is_from_current_player and msg_content == 'going':
        going += 1
      elif len(words) == 2 and words[0] == 'bid' and words[1].isdigit():
        bid = int(words[1])
        bidder = (
            self._id_to_player[msg_author]
            if msg_author in self._id_to_player
            else None)

        if bidder is not None and bid > 0 and bidder.coins >= bid:
          going = 0
          price = bid
          winner_id = msg_author

    return ( winner_id, price )

  def _one_shot(self, current_player, bid_order):
    winner_id = None
    price = None

    while not bid_order.empty():
      player, tag = bid_order.get()

      bid = None
      while bid is None:
        bid = self._io.public_integer(
            f'{tag} what do you bid?', player.id)
        if bid > player.coins:
          self._io.public_send(
              [f'{tag} you cant bid more than you have'])
          bid = None
      if price is None or bid > price:
        price = bid
        winner_id = player.id

    if winner_id == None or price is None or price <= 0:
      winner_id = current_player.id
      price = 0

    return ( winner_id, price )

  def _pricetag(self, current_player_info, bid_order):
    winner_id = None
    price = None

    current_player, current_player_tag = current_player_info

    while price is None:
      price = self._io.public_integer(
          f'{current_player_tag} what is the offering price?',
          current_player.id)
      if price > current_player.coins:
        self._io.public_send([
            f'{current_player_tag} you cant set an asking price '
            'higher than your current holdings.'])
        price = None

    while not bid_order.empty():
      player, tag = bid_order.get()

      if player == current_player:
        winner_id = current_player.id
      elif self._io.public_y_or_no(
          f'{tag} do you want to acquire the emoji for {price}?',
          player):
        winner_id = player.id
        break

    return ( winner_id, price )

  def _proposal(self, cards):
    bids = {}

    cards_text = " and ".join([c.text for c in cards])

    self._io.dm_send(
        [
          ( f'what is your bid for {cards_text}?', p.id )
          for p
          in self._players
        ])

    while len(bids) < len(self._players):
      next_message = self._io.next_message

      if next_message is None:
        continue

      msg_content, msg_author, msg_channel, _ = next_message
      if msg_content.isdigit() and msg_channel[0] == '@':
        bids[msg_author] = int(msg_content)

    winner_id, price = sorted(
        bids.items(), key=lambda _, bid: bid, reverse=True)[0]

  def _select_card_from_hand(
      self,
      prompt,
      player,
      allow_none=False,
      color_restriction=None,
      exclude_doubles=False):
    hand_options = list(
        [
          card
          for card
          in player.hand
          if (
              (color_restriction is None or card.color == color_restriction) and
              (card.sale_type != SaleType.DOUBLE_UP or not exclude_doubles))
        ])
    selection = self._io.private_option(
        prompt,
        [
          f'{i}: {c.text}' for i, c in enumerate(hand_options)
        ] + ([ f'{len(hand_options)}: NONE', ] if allow_none else []),
        player.id)
    card = hand_options[selection] if selection < len(hand_options) else None
    if not allow_none or card is not None:
      player.hand.remove(card)
    return card

  def _send_game_state(self):
    msgs = []
    gallery = []
    for p in self._players:
      hand = "  /  ".join([ card.text for card in p.hand ])
      msgs.append(( f'you have {p.coins}{self.coin_emoji}', p.id ))
      msgs.append(( f'here are the emoji you have to sell: {hand}', p.id ))
      gallery.append(f'{self._io.tag(p.id)}: {p.gallery}')
    self._io.dm_send(msgs)

    gallery_readout = '\n'.join(gallery)
    self._io.public_send(
        [f'here are the emoji everyone has purchased so far:\n{gallery_readout}'])
