import asyncio, logging, multiprocessing
from queue import SimpleQueue

import discord

from keys import Keys

class DiscordIO(object):
  def __init__(self, token):
    self._members = {}
    self._messages = SimpleQueue()
    self._public_channel_id = None
    self._ready = False
    self._room_names = {}
    self._self_id = None
    self._token = token
    self._username = None

    self._inbox = multiprocessing.Queue()
    self._outbox = multiprocessing.Queue()
    self._input_client = multiprocessing.Process(
        target=listen, args=(self._inbox, self._outbox, self._token))
    self._input_client.start()

  @property
  def bot_tag(self):
    return f'<@!{self.self_id}>'

  @property
  def members(self):
    return self._members

  @property
  def next_message(self):
    self._check_inbox()
    if self._messages.qsize() <= 0:
      return None
    return self._messages.get()

  @property
  def public_channel_id(self):
    return self._public_channel_id

  @public_channel_id.setter
  def public_channel_id(self, val):
    if val in self._room_names:
      self._public_channel_id = val
      logging.info(f'Set public channel to {self._room_names[val]}.')

  @property
  def ready(self):
    if not self._ready:
      self._check_inbox()
    return self._ready

  @property
  def room_names(self):
    return self._room_names

  @property
  def self_id(self):
    return self._self_id

  def dm_room(self, pid):
    return self.members[pid][1]

  def dm_send(self, outbox):
    logging.debug(f'DM outbox: {outbox}')
    self._send(
        [
          ( msg, self.dm_room(mid) )
          for msg, mid
          in outbox
          if mid in self.members
        ])

  def get_user_info(self, pid):
    if pid == self.self_id:
      return ( self.bot_tag, self._username )

    if not pid in self.members:
      return None

    return self.members[pid]

  def parse(self, message):
    return (message[0].strip().lower(), message[1], message[2], message[3])

  def private_option(self, prompt, options, player_id):
    self.dm_send(
        [
          ( prompt, player_id ),
          ( '\n'.join(options), player_id ),
        ])

    response = None
    while response is None:
      next_message = self.next_message

      if next_message is None:
        continue

      msg_content, msg_author, msg_channel, _ = self.parse(next_message)
      if (
          msg_author == player_id and
          msg_channel[1] == self.dm_room(player_id) and
          msg_content.isdigit() and
          int(msg_content) in range(len(options))):
        response = int(msg_content)
    return response

  def public_integer(self, prompt, player_id):
    self.public_send([prompt])
    response = None
    while response is None:
      next_message = self.next_message

      if next_message is None:
        continue

      msg_content, msg_author, msg_channel, _ = self.parse(next_message)
      if (
          msg_author == player_id and
          msg_channel[1] == self._public_channel_id and
          msg_content.isdigit()):
        response = int(msg_content)
    return response

  def public_send(self, messages):
    if not self._public_channel_id:
      raise Exception('No public room set.')
    self._send([ ( m, self._public_channel_id ) for m in messages ])

  def public_y_or_no(self, prompt, player):
    self.public_send([prompt])
    response = None
    while response is None:
      next_message = self.next_message

      if next_message is None:
        continue

      msg_content, msg_author, msg_channel, _ = self.parse(next_message)

      if (
          msg_author == player.id and
          msg_channel[1] == self._public_channel_id and
          msg_content in [ 'y', 'n', 'yes', 'no' ]):
        response = msg_content[0] == 'y'
        logging.info('did this happen?')
    logging.info(f'response: {response}')
    return response

  def scrape_id(self, tag):
    try:
      if tag[:3] != '<@!' or tag[-1] != '>':
        return None
      return int(tag[3:-1])
    except:
      return None

  def tag(self, pid):
    members = self.members
    if pid in members:
      return members[pid][0]
    return None

  def _check_inbox(self):
    while not self._inbox.empty():
      p_type, perception = self._inbox.get()

      if p_type == Keys.USERNAME.value:
        self._username, self._self_id = perception
        logging.info(f'Discord username is {self._username}.')
        logging.info(f'Discord ID is {self._self_id}.')
      elif p_type == Keys.READY.value:
        self._ready = True
        logging.info(f'Discord is ready.')
      elif p_type == Keys.CHANNELS.value:
        self._room_names = perception
        logging.info(f'Discord room names: {self.room_names.values()}.')
      elif p_type == Keys.MEMBERS.value:
        self._members = perception
        logging.info(f'Discord members: {list(self.members.keys())}.')
      elif p_type == Keys.MESSAGE.value and self._ready:
        channel_name, channel_id = perception[Keys.CHANNEL.value]

        message = (
            perception[Keys.CONTENT.value],
            perception[Keys.AUTHOR.value],
            perception[Keys.CHANNEL.value],
            perception[Keys.TIMESTAMP.value] )
        if message[1] != self._username:
          self._messages.put(message)

  def _send(self, outbox):
    list(map(self._outbox.put, outbox))

class ListenClient(discord.Client):
  def __init__(self, outbox, inbox):
    self._outbox = outbox
    self._inbox = inbox

    intents = discord.Intents()
    intents.guilds = True
    intents.members = True
    intents.messages = True

    discord.Client.__init__(self, intents=intents)

  async def on_message(self, message):
    channel_name = _make_channel_name(message.channel)
    channel_members = (
        [ message.author.id ]
        if type(message.channel) is discord.DMChannel
        else [
            m.id
            for m
            in message.channel.members
            if m != self.user ])

    msg_author = message.author.id
    msg_content = message.content.strip().lower()

    self._outbox.put(
        (
          Keys.MESSAGE.value,
          {
            Keys.AUTHOR.value: msg_author,
            Keys.CONTENT.value: msg_content,
            Keys.CHANNEL.value: ( channel_name, message.channel.id ),
            Keys.TIMESTAMP.value: message.created_at.timestamp(),
          }
        ))

    logging.info(
        f'Received message from {msg_author} in {channel_name}: {msg_content}')

  async def on_ready(self):
    self._outbox.put(( Keys.USERNAME.value, ( self.user.name, self.user.id )))

    text_channels = {
      channel.id: _make_channel_name(channel)
      for channel
      in self.get_all_channels()
      if type(channel) is discord.TextChannel
    }

    members = {}

    for user in self.users:
      if user.id == self.user.id:
        continue

      try:
        members[user.id] = ( user.mention, (await user.create_dm()).id )
      except:
        logging.warning(f'Could not create DM room for user {user}.')

    self._outbox.put(( Keys.CHANNELS.value, text_channels ))
    self._outbox.put(( Keys.MEMBERS.value, members ))
    self._outbox.put(( Keys.READY.value, True ))
    logging.info('Initialized Discord client.')

    await self.send()

  async def send(self):
    logging.debug('Attempting to send message(s)...')
    messages_by_channel = {}
    logging.debug(f'Inbox is {"" if self._inbox.empty() else "not "}empty.')
    while not self._inbox.empty():
      message, channel_id = self._inbox.get()
      if channel_id not in messages_by_channel:
        messages_by_channel[channel_id] = (
            await self.fetch_channel(channel_id), [] )
      messages_by_channel[channel_id][1].append(message)

    for channel, messages in messages_by_channel.values():
      if channel is None:
        continue

      for msg in messages:
        await channel.send(msg)
        logging.info(f'Sent "{msg}".')

    self.loop.create_task(self.send())

def listen(outbox, inbox, token):
  client = ListenClient(outbox, inbox)
  client.run(token)

def _make_channel_name(channel):
  channel_name = None
  if type(channel) is discord.DMChannel:
    channel_name = f'@{channel.recipient}'
  if type(channel) is discord.TextChannel:
    channel_name = f'{channel.guild.name}#{channel.name}'
  return channel_name
